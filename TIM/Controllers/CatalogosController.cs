﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TIM.Controllers
{
    [Authorize]
    public class CatalogosController : ApiController
    {
        public Catalogos GetCatalogos()
        {
           
            AccesoBD bd = new AccesoBD();
            
            //Descarga de catalogo de usuarios
            #region Usuarios 
            List<Test> Tests = new List<Test>();
            Test TestItem = null;

            List<DbParameter> parameterList = new List<DbParameter>();

            using (DbDataReader dataReader = bd.GetDataReader("sp_GetUsuarios", parameterList, CommandType.StoredProcedure))
            {
                if (dataReader != null)
                {

                    while (dataReader.Read())
                    {

                        TestItem = new Test();
                        TestItem.SlpCode = (int)dataReader["SlpCode"];
                        TestItem.SlpName = (string)dataReader["SlpName"];
                        TestItem.Memo = (string)dataReader["Memo"];
                        TestItem.Usser = (string)dataReader["U_USSER"];
                        TestItem.Password = (string)dataReader["U_PASSWORD"];

                        Tests.Add(TestItem);
                    }
                }
            }
            #endregion

            //Descarga del catálogo de etapas
            #region Etapas  
            List<etapa> etapas = new List<etapa>();
            etapa EtapasItem = null;

            List<DbParameter> parameters = new List<DbParameter>();

            using (DbDataReader dataReader = bd.GetDataReader("sp_GetEtapas", parameters, CommandType.StoredProcedure))
            {
                if (dataReader != null)
                {

                    while (dataReader.Read())
                    {

                        EtapasItem = new etapa();
                        EtapasItem.StageID = (int)dataReader["StageID"];
                        EtapasItem.Name = (string)dataReader["Name"];
                        EtapasItem.Description = (string)dataReader["Dscription"];

                        etapas.Add(EtapasItem);
                    }
                }
            }
            #endregion

            Catalogos catalogos = new Catalogos();
            catalogos.usuarios = Tests;
            catalogos.etapas = etapas;

            return catalogos;
        }
    }
}
