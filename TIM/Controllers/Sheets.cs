﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIM.Controllers
{
    
    public class Sheet
    {
        public int slpCode { get; set; }
        public string SlpName { get;  set; }
        public List<Actividad> Activity { set; get; }
        public Int32 EntryActivity { get; set; }
        public Int32 ActivityType { get;  set; }
        public List<byte[]> images { get; set; }
        public byte[] signature { get; set; }
        public string comments { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
    }
}