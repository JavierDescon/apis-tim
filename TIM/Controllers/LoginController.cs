﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.IO;
using System.Web.Http;
using TIM.Models;
using Microsoft.IdentityModel.Tokens;

namespace TIM.Controllers
{
    [AllowAnonymous]
    public class LoginController : ApiController
    {
        List<Login> listaUsuarios= new List<Login>();
        public LoginController()
        {
           
            //Usuarios con acceso, para generacion de token
            listaUsuarios.Add(new Login("Javier", "jav3097."));
            listaUsuarios.Add(new Login("Claudia", "Admin123"));
        }

        [HttpPost]
        public IHttpActionResult Authenticate([FromBody] Login usuario)
        {
            var use = listaUsuarios.SingleOrDefault(x => x.nombre == usuario.nombre && x.password == usuario.password);

            if (use != null)
            {
                string token = createToken(usuario.nombre);
                //retorna el token generado
                return Ok<string>(token);
            }
            else
            {
                return Ok<string>("Usuario y/o contraseña incorrecta");
            }
        }

        private string createToken(string username)
        {
            DateTime issuedAt = DateTime.UtcNow;
            DateTime expires = DateTime.UtcNow.AddDays(7);
            var tokenHandler = new JwtSecurityTokenHandler();
            
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, username)
            });

            //Se definen los paramtros de seguridad del token
            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);


            //Crea el Token
            var token =tokenHandler.CreateJwtSecurityToken(issuer: "issuer", audience: "issuer",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
    }
}
