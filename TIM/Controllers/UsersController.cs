﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TIM.Controllers
{
    [Authorize]
    public class UsersController : ApiController
    {
        public Usuario GetTodosUsuarios()
        {
            AccesoBD bd = new AccesoBD();
           
            string json = "";
            List<Test> Tests = new List<Test>();
            Test TestItem = null;

            List<DbParameter> parameterList = new List<DbParameter>();

            using (DbDataReader dataReader = bd.GetDataReader("sp_GetUsuarios", parameterList, CommandType.StoredProcedure))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        TestItem = new Test();
                        TestItem.SlpCode = (int)dataReader["SlpCode"];
                        TestItem.SlpName = (string)dataReader["SlpName"];
                        TestItem.Memo = (string)dataReader["Memo"];
                        TestItem.Usser = (string)dataReader["U_USSER"];
                        TestItem.Password = Encrypt.Base64_Encode((string)dataReader["U_PASSWORD"]);

                        Tests.Add(TestItem);
                    }
                }
            }

            Usuario usuario = new Usuario();
            usuario.usuarios = Tests;

            json = JsonConvert.SerializeObject(usuario);
            object res = json;

            return usuario;
        }
    }

    public class Encrypt
    {
        //codificar base64
        public static string Base64_Encode(string str)
        {
            byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(str);
            return Convert.ToBase64String(encbuff);
        }

        //Decodificar base64
        public static string Base64_Decode(string str)
        {
            try
            {
                byte[] decbuff = Convert.FromBase64String(str);
                return System.Text.Encoding.UTF8.GetString(decbuff);
            }
            catch
            {
                //si se envia una cadena si codificación base64, mandamos vacio
                return "";
            }
        }
    }

    public class Test
    {
        public object SlpCode { get; internal set; }
        public object SlpName { get; internal set; }
        public object Memo { get; internal set; }
        public object Password { get; internal set; }
        public object Usser { get; internal set; }
    }
}
