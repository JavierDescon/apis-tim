﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TIM.Controllers
{
    [Authorize]
    public class ActividadesController : ApiController
    {
        public Actividades GetActividades(int Slpcode)
        {
            
            AccesoBD bd = new AccesoBD();


            List<Act> Tests = new List<Act>();
            Act TestItem = null;

            List<DbParameter> parameterList = new List<DbParameter>();
            parameterList.Add(new SqlParameter("@SlpCode", Slpcode));

            using (DbDataReader dataReader = bd.GetDataReader("sp_GetActividades", parameterList, CommandType.StoredProcedure))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        TestItem = new Act();
                        TestItem.Id = (int)dataReader["ID"];
                        TestItem.CardCode = (string)dataReader["CARDCODE"];
                        TestItem.CardName = (string)dataReader["CARDNAME"];
                        TestItem.Activity_Name = (string)dataReader["NOMBRE ACTIVIDAD"];
                        TestItem.Contact = (string)dataReader["CONTACTO"];

                        Tests.Add(TestItem);
                    }
                }
            }
            Actividades actividades = new Actividades();
            actividades.actividades = Tests;

            return actividades;
        }
    }
}
