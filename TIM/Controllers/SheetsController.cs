﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace TIM.Controllers
{
    [Authorize]
    public class SheetsController : ApiController
    {
        public string sErrMsg;
        public int lErrCode;
        public int lRetCode;
        public Boolean Conectado = false;
        public static SAPbobsCOM.Company oCompany;
        public string server = Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["ServidorSQL"]);
        public string LicenseServer = Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["LicenseServer"]);
        public string DbUserName = Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["UsuarioSQL"]);
        public string DbPassword = Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["PasswordSQL"]);
        public string empresa1 = Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["empresa"]);
        public string usuario1 = Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["usuario"]);
        public string pass1 = Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["pass"]);
        public string TipoSQL = Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["TipoSQL"]);
        public string Path= Encrypt.Base64_Decode(ConfigurationSettings.AppSettings["Path"]);

        [HttpPost]
        public Resultado PostSheets(Sheet json)
        {
            AccesoBD bd = new AccesoBD();


            int empresa = 0;
            empresa = 1;
            Resultado response = new Resultado();
            //var respuesta = response;
            if (Conectado == false)
            {
                var respuesta = ConectaSAP(empresa);
                if (respuesta.error == false)
                {
                    try
                    {
                        byte[] img=null;
                        string proyecto = "";
                        string SlpName = json.SlpName;
                        string start = "";
                        string end = "";
                        DateTime startDate = DateTime.Now;
                        DateTime endDate = DateTime.Now;
                        string nonBillableTime = "";
                        List<Actividad> actividads = new List<Actividad>();
                        actividads = json.Activity;
                        foreach (Actividad item in actividads)
                        {
                            DateTime dateTime = DateTime.Now;
                            DateTime dateTime2 = DateTime.Now;
                            dateTime = Convert.ToDateTime(item.startdate);
                            dateTime2 = Convert.ToDateTime(item.endDate);
                            start = item.startTime;
                           end = item.endTime;
                            startDate = dateTime;//.ToString("MM/dd/yyyy");
                           endDate = dateTime2;//.ToString("MM/dd/yyyy");
                           nonBillableTime = item.nonBillableTime;
                        }
                        string[] dividir;
                        dividir = start.Split(':');
                        double horas = Convert.ToDouble(dividir[0]);
                        double minutos= Convert.ToDouble(dividir[1]);

                        string[] dividir2;
                        dividir2 = end.Split(':');
                        double horasE = Convert.ToDouble(dividir2[0]);
                        double minutosE = Convert.ToDouble(dividir2[1]);


                        int entryActivity = json.EntryActivity;
                        int ActivityType = json.ActivityType;
                        string comentarios = json.comments;
                        decimal latitud = json.latitude;
                        decimal longitud = json.longitude;
                        int f;
                        for (f = 0; f <= json.images.Count - 1; f++)
                        {
                            List<byte[]> imagenes = new List<byte[]>();

                            imagenes = json.images;
                            int x;
                            for(x=0;x <= imagenes.Count -1; x++)
                            {
                                img = imagenes[x];
                            string ruta = HttpContext.Current.Server.MapPath("/imagenes") + "\\imagen_" + f + ".png";
                             System.IO.MemoryStream ms = new System.IO.MemoryStream(img);
                             System.Drawing.Bitmap b = (System.Drawing.Bitmap)Image.FromStream(ms);
                             b.Save( ruta, ImageFormat.Png);
                            }
                           
                           
                        }

                        //guardamos la firma en un repositorio 
                        byte[] firma = json.signature;
                        string ruta2 = HttpContext.Current.Server.MapPath("/imagenes") + "\\firma.png";
                        System.IO.MemoryStream ms2 = new System.IO.MemoryStream(firma);
                        System.Drawing.Bitmap b2 = (System.Drawing.Bitmap)Image.FromStream(ms2);
                        b2.Save(ruta2, System.Drawing.Imaging.ImageFormat.Png);

                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(new SqlParameter("@AbsEntry", entryActivity));

                using (DbDataReader dataReader = bd.GetDataReader("sp_GetProyecto", parameterList, CommandType.StoredProcedure))
                {
                    if (dataReader != null)
                    {

                        while (dataReader.Read())
                        {
                            proyecto= (string)dataReader["FIPROJECT"];
                        }
                    }
                }

                        DateTime fecha1 = startDate;
                        int mes = fecha1.Month;
                        int año = fecha1.Year;

                        int absEntry = 0;
                        //buscamos si existe un cronograma del empleado
                        string ConsultaSAP = "SELECT absEntry from OTSH  where UserID=" + json.slpCode +" and DateFrom between '"+ año +"-"+ mes + "-01 00:00:00.000' and '" + año + "-" + mes +"-31 00:00:00.000' ";
                        using (SqlConnection cn = new SqlConnection(bd.ConnectionString))
                        {
                            cn.Open();

                            SqlCommand cmd = new SqlCommand(ConsultaSAP, cn);
                            SqlDataReader dr = cmd.ExecuteReader();

                            if (dr.Read())
                            {
                                absEntry =Convert.ToInt16( dr["absEntry"]);
                            }
                        }
                                              
                       
                        DateTime startTime, endTime, breakTime;
                        startTime = Convert.ToDateTime(startDate);//startTime
                        startTime = startTime.AddHours(horas);
                        startTime = startTime.AddMinutes(minutos);
                        endTime = Convert.ToDateTime(startDate);//EndTime
                        endTime = endTime.AddHours(horasE);
                        endTime = endTime.AddMinutes(minutosE);

                        //Actualización de -sheet
                        if (absEntry != 0)
                        {

                            SAPbobsCOM.CompanyService oCompanyServices = default(SAPbobsCOM.CompanyService);
                            SAPbobsCOM.ProjectManagementTimeSheetService oTimeSheetServices = null;
                            oCompanyServices = oCompany.GetCompanyService();
                            oTimeSheetServices = (SAPbobsCOM.ProjectManagementTimeSheetService)oCompanyServices.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementTimeSheetService);
                            SAPbobsCOM.PM_TimeSheetParams oTimeSheetParams = (SAPbobsCOM.PM_TimeSheetParams)oTimeSheetServices.GetDataInterface(SAPbobsCOM.ProjectManagementTimeSheetServiceDataInterfaces.pmtssPM_TimeSheetParams);
                            oTimeSheetParams.AbsEntry = absEntry;
                            SAPbobsCOM.PM_TimeSheetData oTimeSheets = oTimeSheetServices.GetTimeSheet(oTimeSheetParams);
                            SAPbobsCOM.PM_TimeSheetLineData oTimeSheetLines = oTimeSheets.PM_TimeSheetLineDataCollection.Add();

                                                    

                            
                            oTimeSheetLines.Date =  startDate;//Date                          
                            oTimeSheetLines.StartTime = startTime;
                            oTimeSheetLines.EndTime = endTime;
                            //oTimeSheetLine.Break = breakTime;
                            //oTimeSheetLine.NonBillableTime = Convert.ToDateTime(nonBillableTime);
                            //oTimeSheetLines.ActivityType = ActivityType;//ActivityType
                            oTimeSheetLines.CostCenter = "";//CostCenter
                            oTimeSheetLines.FinancialProject = proyecto;//FinancialProject

                            oTimeSheetServices.UpdateTimeSheet(oTimeSheets);
                            oTimeSheetLines = oTimeSheets.PM_TimeSheetLineDataCollection.Add();
                            
                           
                           
                             
                            response.error = false;
                            response.message = "Registro insertado correctamente";
                            response.EntryActivity = entryActivity;

                        }
                        //Creación de sheet
                        else
                        {
                            SAPbobsCOM.CompanyService oCompanyService = default(SAPbobsCOM.CompanyService);
                            SAPbobsCOM.ProjectManagementTimeSheetService oTimeSheetService = null;
                            oCompanyService = oCompany.GetCompanyService();
                            oTimeSheetService = (SAPbobsCOM.ProjectManagementTimeSheetService)oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementTimeSheetService);
                            SAPbobsCOM.PM_TimeSheetData oTimeSheet = (SAPbobsCOM.PM_TimeSheetData)oTimeSheetService.GetDataInterface(SAPbobsCOM.ProjectManagementTimeSheetServiceDataInterfaces.pmtssPM_TimeSheetData);
                            SAPbobsCOM.PM_TimeSheetParams oTimeSheetParam = (SAPbobsCOM.PM_TimeSheetParams)oTimeSheetService.GetDataInterface(SAPbobsCOM.ProjectManagementTimeSheetServiceDataInterfaces.pmtssPM_TimeSheetParams);

                            SAPbobsCOM.PM_TimeSheetLineData oTimeSheetLine = oTimeSheet.PM_TimeSheetLineDataCollection.Add();

                            oTimeSheet.TimeSheetType = SAPbobsCOM.TimeSheetTypeEnum.tsh_Employee;

                        oTimeSheet.UserID = json.slpCode;
                        oTimeSheet.FirstName = SlpName;//slpName
                        oTimeSheet.LastName = SlpName;
                            oTimeSheet.DateFrom = Convert.ToDateTime(mes +  "-01" + "-" + año + "");
                            if (mes == 28)
                            {
                                oTimeSheet.Dateto = Convert.ToDateTime(mes + "-28" + "-" + año + "");
                            }
                            else
                            {
                                oTimeSheet.Dateto = Convert.ToDateTime(mes  +  "-30" + "-" + año + "");
                            }
                            //breakTime = DateTime.Today; //breakTime
                            //breakTime = breakTime.AddMinutes(45);
                            //nonBillableTime = DateTime.Today; //nonBillableTime
                            //nonBillableTime = nonBillableTime.AddHours(1);
                           
                        oTimeSheetLine.Date = startDate;//Date                          
                        oTimeSheetLine.StartTime =startTime ;
                        oTimeSheetLine.EndTime = endTime;
                        //oTimeSheetLine.Break = breakTime;
                        //oTimeSheetLine.NonBillableTime = Convert.ToDateTime(nonBillableTime);
                        //oTimeSheetLine.ActivityType = 0;//ActivityType
                        oTimeSheetLine.CostCenter = "";//CostCenter
                        oTimeSheetLine.FinancialProject = proyecto;//FinancialProject              


                        oTimeSheetParam = oTimeSheetService.AddTimeSheet(oTimeSheet);
                        response.error = false;
                        response.message = "Registro insertado correctamente";
                        response.EntryActivity = entryActivity;
                        }
                    }
                    catch(Exception ex)
                    {
                        response.error = true;
                        response.message = ex.Message;
                        response.EntryActivity = 0;
                    }
                }
                else
                {
                    response.error = respuesta.error;
                    response.message = respuesta.message;
                }

            }
            return response;
        }

        public Resultado ConectaSAP(int tipo)
        {
            Resultado response = new Resultado();
            try
            {
                oCompany = new SAPbobsCOM.Company();
                oCompany.Server = server;
                oCompany.LicenseServer = LicenseServer;
                oCompany.DbUserName = DbUserName;
                oCompany.DbPassword = DbPassword;
                
                if (TipoSQL == "2008")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008);
                }
                else if (TipoSQL == "2012")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012);
                }
                else if (TipoSQL == "2014")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014);
                }
                else if (TipoSQL == "2016")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016);
                }

                oCompany.UseTrusted = false;
                if (tipo == 1)
                {
                    oCompany.CompanyDB = empresa1;
                    oCompany.UserName = usuario1;
                    oCompany.Password = pass1;
                }


                // Connecting to a company DB
                lRetCode = oCompany.Connect();

                if (lRetCode != 0)
                {
                    int temp_int = lErrCode;
                    string temp_string = sErrMsg;
                    oCompany.GetLastError(out temp_int, out temp_string);
                    //MessageBox.Show(globals_Renamed.sErrMsg);

                    response.error = true;
                    response.message = sErrMsg;
                }
                else
                {
                    response.error = false;
                    response.message = "Conectado";

                }
            }
            catch (Exception ex)
            {
                response.error = true;
                response.message = ex.Message;
            }
          
            return response;
        }
    }
}