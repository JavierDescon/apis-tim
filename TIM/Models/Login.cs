﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIM.Models
{
    public class Login
    {
        public string nombre { get; set; }
        public string password { get; set; }

        public Login(string username, string password)
        {
            this.nombre = username;
            this.password = password;
        }
    }
}